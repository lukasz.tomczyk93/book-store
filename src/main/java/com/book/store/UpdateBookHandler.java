package com.book.store;

import java.util.Scanner;

class UpdateBookHandler implements OptionHandler {

    @Override
    public void handle() {
        var scanner = new Scanner(System.in);
        System.out.println("Enter the title of book to update: ");
        String theTitle = scanner.nextLine();
        Book newBookData = BookStoreUtils.getBookData(scanner);

        for (int i = 0; i < BooksDatabase.books.length; i++) {
            if (BooksDatabase.books[i].title().equalsIgnoreCase(theTitle)) {
                BooksDatabase.books[i] = newBookData;
                break;
            }
        }
    }
}

