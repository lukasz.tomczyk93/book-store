package com.book.store;

enum BookApplicationMenu {
    ADD_BOOK(1, new AddBookHandler()),
    UPDATE_BOOK(2, new UpdateBookHandler()),
    GET_ALL_BOOKS(3, new GetAllBooksHandler()),
    GET_BOOK_BY_TITLE(4, new GetBookByTitleHandler()),
    DELETE_BOOK_BY_TITLE(5, new DeleteBookByTitleHandler()),
    UNSUPPORTED(Integer.MIN_VALUE, new UnsupportedOptionHandler());

    private final int optionValue;
    private final OptionHandler optionHandler;

    BookApplicationMenu(int optionValue, OptionHandler optionHandler) {
        this.optionValue = optionValue;
        this.optionHandler = optionHandler;
    }

    void handleOption() {
        optionHandler.handle();
    }

    static BookApplicationMenu of(int optionValue) {
        for (BookApplicationMenu option : values()) {
            if (option.optionValue == optionValue) {
                return option;
            }
        }
        return UNSUPPORTED;
    }
}
