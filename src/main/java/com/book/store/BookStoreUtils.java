package com.book.store;

import java.util.Scanner;

class BookStoreUtils {

    static Book getBookData(Scanner scanner) {
        System.out.print("Enter ISBN: ");
        String ISBN = scanner.nextLine();
        System.out.print("Enter new title: ");
        String title = scanner.nextLine();
        System.out.print("Enter author: ");
        String author = scanner.nextLine();
        System.out.print("Enter pages: ");
        var pages = scanner.nextInt();

        return new Book(ISBN, title, author, pages);
    }
}
