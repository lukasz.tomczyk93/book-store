package com.book.store;

import java.util.Scanner;

class AddBookHandler implements OptionHandler {

    private static final int INITIAL_CAPACITY = 1;

    @Override
    public void handle() {
        var scanner = new Scanner(System.in);
        var newBook = BookStoreUtils.getBookData(scanner);

        if (BooksDatabase.books.length == 0) {
            BooksDatabase.books = initBooksDatabase();
        } else {
            BooksDatabase.books = resizeBooksArray(BooksDatabase.books);
        }

        var nextFreeIndex = BooksDatabase.books.length - 1;

        BooksDatabase.books[nextFreeIndex] = newBook;
        System.out.println();
    }

    private Book[] initBooksDatabase() {
        return new Book[INITIAL_CAPACITY];
    }

    private Book[] resizeBooksArray(Book[] books) {
        Book[] resizedArray = new Book[books.length + 1];
        for (int i = 0; i < books.length; i++) {
            resizedArray[i] = books[i];
        }
        books = resizedArray;
        return books;
    }
}
