package com.book.store;

class GetAllBooksHandler implements OptionHandler {

    @Override
    public void handle() {
        System.out.println();
        System.out.println("All books in store: ");
        Book[] books = BooksDatabase.books;
        for (Book book : books) {
            if (book != null) {
                System.out.println(book);
            }
        }
        System.out.println();
    }
}
