package com.book.store;

import java.util.Scanner;

class GetBookByTitleHandler implements OptionHandler {

    @Override
    public void handle() {
        var scanner = new Scanner(System.in);
        System.out.println("Enter a title: ");
        var title = scanner.nextLine();

        for (Book currentBook : BooksDatabase.books) {
            boolean isBookNotNull = currentBook != null;
            if (isBookNotNull && currentBook.title().equalsIgnoreCase(title)) {
                System.out.println(String.format("Found book: %s", currentBook));
                break;
            }
        }
        System.out.println(String.format("Couldn't find book by title '%s'", title));
    }
}
