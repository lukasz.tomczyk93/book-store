package com.book.store;

import java.util.Scanner;

class BookStoreApplication {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        int userOptionNumber;
        BookApplicationMenu option;

        do {
            userOptionNumber = getUserOptionNumber(scanner);
            option = BookApplicationMenu.of(userOptionNumber);
            handleUserOption(option);
        } while (option != BookApplicationMenu.UNSUPPORTED);
    }

    private static int getUserOptionNumber(Scanner scanner) {
        System.out.println("1 - add book");
        System.out.println("2 - update book");
        System.out.println("3 - get all books");
        System.out.println("4 - get book by title");
        System.out.println("5 - delete book");
        System.out.println("(Type any other number to quit)");
        System.out.print("Choose option: ");
        return scanner.nextInt();
    }

    private static void handleUserOption(BookApplicationMenu option) {
        option.handleOption();
    }
}
