package com.book.store;

import java.util.Scanner;

class DeleteBookByTitleHandler implements OptionHandler {

    @Override
    public void handle() {
        var scanner = new Scanner(System.in);
        System.out.println("Which book would you like to delete?");
        var bookTitle = scanner.nextLine();

        boolean bookWasFound = false;
        for (int i = 0; i < BooksDatabase.books.length; i++) {
            if (bookTitle.equalsIgnoreCase(BooksDatabase.books[i].title())) {
                BooksDatabase.books[i] = null;
                System.out.println("This book has been deleted");
                compactDatabase();
                bookWasFound = true;
                break;
            }
        }

        if (!bookWasFound) {
            System.out.println("This book has been not found.");
        }
    }

    private static void compactDatabase() {
        var newArraySize = BooksDatabase.books.length - 1;
        Book[] compactedArray = new Book[newArraySize];

        int j = 0;
        for (int i = 0; i < BooksDatabase.books.length; i++) {
            if (BooksDatabase.books[i] != null) {
                compactedArray[j] = BooksDatabase.books[i];
                j++;
            }
        }

        BooksDatabase.books = compactedArray;
    }
}


