package com.book.store;

class UnsupportedOptionHandler implements OptionHandler {

    @Override
    public void handle() {
        System.out.println("By bye...");
    }
}
